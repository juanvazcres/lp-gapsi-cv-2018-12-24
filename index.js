var express = require('express');
var app = express();

app.set('views', './components');
app.set('view engine', 'pug');
app.use('/includes', express.static('includes'));
app.get('/', function (req, res) {
  res.render('index', { name: 'ALGO' })
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});